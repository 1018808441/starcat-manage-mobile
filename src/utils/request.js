/* eslint-disable */
import axios from 'axios'
import Qs from 'qs'

// 请求拦截器
axios.interceptors.request.use(function (config) {
	let login = localStorage.getItem('login')
	login = JSON.parse(login)
	const token = login ? login.token : null;
    if(token){
      config.headers['Authorization'] = 'Bearer ' + token;
    }
    return config;
}, function (error) {
    return Promise.reject(error);
});
// 响应拦截器
axios.interceptors.response.use(function (response) {
  if (response.config.responseType == "blob") {
    return response
  } else {
    return response.data
  }
}, function (error, data) {
	if (error.response) {
		if (error.response.status === 401) {
			location.href = '/login'
			return
		}

    error.response.data.code = error.response.status
    error.response.data.msg = 'Http Code Error!'
		return error.response.data
	} else {
		return Promise.reject(error)
	}
});

// import { LoginMethod } from '@/utils/request.js'
export const LoginMethod = (data) => {return axios.post('/manage/mmobile/login', Qs.stringify(data))}
export const AgentMethod = (data) => {return axios.post('/manage/mmobile/agent', Qs.stringify(data))}
export const AgentFhMethod = (data) => {return axios.post('/manage/mmobile/agent-fh', Qs.stringify(data))}
export const AgentDxMethod = (data) => {return axios.post('/manage/mmobile/agent-dx', Qs.stringify(data))}
export const AgentSetMethod = (data) => {return axios.post('/manage/mmobile/agent-set', Qs.stringify(data))}
export const AgentTypeMethod = (data) => {return axios.post('/manage/mmobile/agent-type', Qs.stringify(data))}
export const AgentAddMethod = (data) => {return axios.post('/manage/mmobile/agent-add', Qs.stringify(data))}
export const StoreMethod = (data) => {return axios.post('/manage/mmobile/store', Qs.stringify(data))}
export const DataMethod = (data) => {return axios.post('/manage/mmobile/data', Qs.stringify(data))}
