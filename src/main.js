// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import NutUI from '@nutui/nutui';
import '@nutui/nutui/dist/nutui.css';

router.beforeEach((to, from, next) => {
	if (to.path != '/login') {
		// auto login
		let data = localStorage.getItem('login')
		if (data) {

		} else {
			next({ path: 'login' })
		}
	}
	next()
})

Vue.prototype.pagePageSize = 10
Vue.config.productionTip = false
NutUI.install(Vue)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
