import Vue from 'vue'
import Router from 'vue-router'
// import Index from '@/pages/index/index'

import Record from '@/pages/record/index'
import Login from '@/pages/Login/index'
import Login2 from '@/pages/Login/index2'
import BonusAgent from '@/pages/bonusAgent/index'
// import SalesAgent from '@/pages/salesAgent/index'
import SalesAgent from '@/pages/store-tab'
import Shop from '@/pages/Shop/index'
import Dividend from '@/pages/addtype/dividend'
import Sales from '@/pages/addtype/sales'
Vue.use(Router)
//showHeader是否显示顶部
//showImg是否显示顶部左侧返回图片

export default new Router({
  mode: 'history',
  routes: [{
      path: '/',
      name: 'Record',
      meta:{
          showHeader:true,
          showImg:false,
      },
      component: Record
    },
    {
      path: '/login',
      name: 'login',
      meta:{
        showHeader:false,
        showImg:false,
      },
      component: Login
    },
    {
      path: '/login2',
      name: 'login2',
      meta:{
        showHeader:false,
        showImg:false,
      },
      component: Login2
    },
    {
      path: '/bonusAgent',
      name: 'bonusAgent',
      meta:{
        showHeader:true,
        showImg:false,
      },
      component: BonusAgent
    },
    {
      path: '/salesAgent',
      name: 'salesAgent',
      meta:{
        showHeader:true,
        showImg:false,
      },
      component: SalesAgent
    },
    {
      path: '/shop',
      name: 'shop',
      meta:{
        showHeader:true,
        showImg:true,
      },
      component: Shop
    },
    {
      path: '/dividend',
      name: 'dividend',
      meta:{
        showHeader:true,
        showImg:true,
      },
      component: Dividend
    }, {
      path: '/sales',
      name: 'sales',
      meta:{
        showHeader:true,
        showImg:true,
      },
      component: Sales
    },

  ]
})
